<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests;

class SubTaskHandlerController extends TaskHandlerController
{
    /**
	* create new sub task 
	*
	* @return redirects to list of tasks
    **/
    public function createSubTask($parent, Request $request){
    	
    	//create a new sub task
    	$this->storeSubTask($parent, null, $request);

        return redirect('/');
    }

    /**
	* update sub task 
	*
	* @return redirects to list of tasks
    **/
    public function saveSubTask($parent, $id, Request $request){
    	
    	//update a sub task
    	$this->storeSubTask($parent, $id, $request);

        return redirect('/');
    }

    /**
	* store the subtask 
	*
	* @param parent task
	* @param optional id of task
	* @return return a new task or updated task
    **/
    public function storeSubTask($parent, $id, Request $request){
    	
    	$task = parent::store($id, $request);

    	$task->parent_task_id = $parent;
        if(isset($request->completed)){
            $task->completed = true;
        }else{
            $task->completed = false;
        }
    	$task->save();
        
        SubTasksHandlerController::updateParentTaskCompletedStatus($parent);

        return $task;
    }
}
