<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests;

class SubTasksHandlerController extends Controller
{
	/**
	* loads the list of parent tasks 
	*
	* @return view list of parent tasks
    **/
    public function index($id){

        $tasks = Task::where('parent_task_id','=', null)->orderBy('created_at', 'asc')->get();
        $subTasks = SubTasksHandlerController::fetchSubTasksFromParentTasks($tasks);

    	return view('tasks', [
            'tasks' => $tasks,
            'subTasks' => $subTasks,
            'taskForShowSubTaskForm' => $id
        ]);
    }

    /**
    * edit a sub task 
    *
    * @return loads the list of task with the edit form
    **/
    public function edit($id){
        
        $tasks = Task::where('parent_task_id','=', null)->orderBy('created_at', 'asc')->get();
        $subTasks = SubTasksHandlerController::fetchSubTasksFromParentTasks($tasks);

        return view('tasks', [
            'tasks' => $tasks,
            'subTasks' => $subTasks,
            'edit' => true,
            'child_id' => $id
        ]);
    }

    /**
	* loads the list of child tasks 
	*
	* @return list of child tasks
    **/
    public static function fetchSubTasksFromParentTasks($tasks){
    	$subTasks = [];

        foreach ($tasks as $task) {
            $queriedSubTasks = Task::where('parent_task_id','=', $task->id)->orderBy('created_at', 'asc')->get();

            if(count($queriedSubTasks) > 0){
                $subTasks[$task->id] = $queriedSubTasks;
            }
        }

    	return $subTasks;
    }

    /**
    * update parent task based on all subtasks status 
    *
    * @return true or false if gets updated
    **/
    public static function updateParentTaskCompletedStatus($id){
        $task = Task::findOrFail($id);

        $queriedSubTasks = Task::where('parent_task_id','=', $task->id)->orderBy('created_at', 'asc')->get();

        $totalSubTasks = count($queriedSubTasks);

        if($totalSubTasks > 0){
            $completedSubTasks = 0;
            foreach ($queriedSubTasks as $subTasks) {
                if($subTasks->completed == true || $subTasks->completed == 1){
                    $completedSubTasks++;
                }
            }

            if($totalSubTasks == $completedSubTasks){
                $task->completed = true;
            }else{
                $task->completed = false;
            }
        }else{
            $task->completed = false;
        }

        return $task->save();
    }
}
