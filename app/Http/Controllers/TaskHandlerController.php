<?php

namespace App\Http\Controllers;

use Validator;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests;

class TaskHandlerController extends Controller
{
    /**
	* create new task 
	*
	* @return redirects to list of tasks
    **/
    public function create(Request $request){
    	
    	//create a new task
    	$this->store(null, $request);

        return redirect('/');
    }

    /**
	* save task 
	*
	* @return redirects to list of tasks
    **/
    public function save($id, Request $request){
    	
    	//update a stored task
    	$this->store($id, $request);

        return redirect('/');
    }

    /**
	* store the task 
	*
	* @param optional id of task
	* @return return a new task or updated task
    **/
    public function store($id, Request $request){
    	$validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        if(isset($id)){
        	$task = Task::findOrFail($id);
        }else{
        	$task = new Task;
        }

        $task->name = $request->name;
        $task->save();

        return $task;
    }

    /**
	* delete a task 
	*
	* @return redirects to list of tasks
    **/
    public function delete($id){
    	Task::findOrFail($id)->delete();

        return redirect('/');
    }
}
