<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\SubTasksHandler;

class TasksHandlerController extends Controller
{
    /**
	* loads the list of parent tasks 
	*
	* @return view list of parent tasks
    **/
    public function index(){

        $tasks = Task::where('parent_task_id','=', null)->orderBy('created_at', 'asc')->get();
        $subTasks = SubTasksHandlerController::fetchSubTasksFromParentTasks($tasks);
        
        // dd($subTasks);
        
    	return view('tasks', [
            'tasks' => $tasks,
            'subTasks' => $subTasks
        ]);
    }

    /**
    * edit a task 
    *
    * @return loads the list of task with the edit form
    **/
    public function edit($id){
        
        $tasks = Task::where('parent_task_id','=', null)->orderBy('created_at', 'asc')->get();
        $subTasks = SubTasksHandlerController::fetchSubTasksFromParentTasks($tasks);

        return view('tasks', [
            'tasks' => $tasks,
            'subTasks' => $subTasks,
            'edit' => true,
            'id' => $id
        ]);
    }

}
