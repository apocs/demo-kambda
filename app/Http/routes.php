<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use App\Task;
use Illuminate\Http\Request;

Route::group(['middleware' => ['web']], function () {

    // TASKS

    /**
     * Show Task Dashboard
     */
    Route::get('/', 'TasksHandlerController@index');

    /**
    * Show Edit form task
    */
    Route::get('/task/edit/{id}', 'TasksHandlerController@edit');

    /**
     * Add New Task
     */
    Route::post('/task', 'TaskHandlerController@create');

    /**
     * Store Task
     */
    Route::post('/task/{id}', 'TaskHandlerController@save');

    /**
     * Delete Task
     */
    Route::delete('/task/{id}', 'TaskHandlerController@delete');

    // SUBTASKS

    /**
     * Show Create SubTask Form
     */
    Route::get('/task/{id}/subtask', 'SubTasksHandlerController@index');

    /**
     * Show Edit SubTask Form
     */
    Route::get('/subtask/edit/{id}', 'SubTasksHandlerController@edit');

    /**
     * Add New Sub Task
     */
    Route::post('/task/{parent}/subtask', 'SubTaskHandlerController@createSubTask');

    /**
     * Store Sub Task
     */
    Route::post('/task/{parent}/subtask/{id}', 'SubTaskHandlerController@saveSubTask');
});
