@if(isset($taskForShowSubTaskForm))
    <form action="/task/{{ $currentTask->id }}/subtask" method="POST" class="form-horizontal">
        <td>
            <i class="fa fa-long-arrow-right"></i>
        </td>
        <td>
            <input type="checkbox" name="completed" id="task-completed"/>
        </td>
        {{ csrf_field() }}
        <td class="table-text"> 
            <div>
                <input type="text" name="name" id="task-name" class="form-control" />
            </div>
        </td>
        <!-- Task Options Buttons -->
        <td>
            <button type="submit" class="btn btn-sm btn-success">
                <i class="fa fa-btn fa-check"></i>Create
            </button>
        </td>
        <td>
            <a href="/" class="btn btn-sm btn-danger">
                <i class="fa fa-btn fa-close"></i>Cancel
            </a>
        </td>
    </form>
    <td></td>
@else
    <td>
        <i class="fa fa-long-arrow-right"></i>
    </td>
    <form action="/task/{{ $currentTask->parent_task_id }}/subtask/{{ $currentTask->id }}" method="POST" class="form-horizontal">
        <td>
            @if($currentTask->completed == 1)
                <input type="checkbox" name="completed" value="true" checked="checked" />
            @else
                <input type="checkbox" name="completed" value="false" />
            @endif
            
        </td>
        {{ csrf_field() }}
        <td class="table-text"> 
            <div>
                <input type="text" name="name" id="task-name" class="form-control" value="{{ $currentTask->name }}" />
            </div>
        </td>
        <!-- Task Options Buttons -->
        <td>
            <button type="submit" class="btn btn-sm btn-success">
                <i class="fa fa-btn fa-check"></i>Save
            </button>
        </td>
        <td>
            <a href="/" class="btn btn-sm btn-danger">
                <i class="fa fa-btn fa-close"></i>Cancel
            </a>
        </td>
    </form>
    <td></td>
@endif