<td>
    <input type="checkbox" name="completed" id="task-completed" disabled="disabled" value="{{ $task->completed }}"/>
</td>
<form action="/task/{{ $task->id }}" method="POST" class="form-horizontal">
    {{ csrf_field() }}
    <td class="table-text"> 
        <div>
            <input type="text" name="name" id="task-name" class="form-control" value="{{ $task->name }}" />
        </div>
    </td>
    <!-- Task Options Buttons -->
    <td>
        <button type="submit" class="btn btn-sm btn-success">
            <i class="fa fa-btn fa-check"></i>Save
        </button>
    </td>
    <td>
        <a href="/" class="btn btn-sm btn-danger">
            <i class="fa fa-btn fa-close"></i>Cancel
        </a>
    </td>
</form>
<td></td>