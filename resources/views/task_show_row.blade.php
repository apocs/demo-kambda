
@if($currentTask->parent_task_id != null)
    <td>
        <i class="fa fa-long-arrow-right"></i>
    </td>
    <td>
        @if($currentTask->completed == 1)
            <input type="checkbox" name="completed" value="true" checked="checked" disabled="disabled"/>
        @else
            <input type="checkbox" name="completed" value="false" disabled="disabled"/>
        @endif
    </td>
@else
    <td>
        @if($currentTask->completed == 1)
            <input type="checkbox" name="completed" value="true" checked="checked" disabled="disabled" />
        @else
            <input type="checkbox" name="completed" value="false" disabled="disabled" />
        @endif
    </td>
    <td></td>
@endif

<td class="table-text"> 
    <div>{{ $currentTask->name }}</div>
</td>
<!-- Task Options Buttons -->
@if($currentTask->parent_task_id != null)
<td>
    <a href="/subtask/edit/{{ $currentTask->id }}" class="btn btn-sm btn-info">
        <i class="fa fa-btn fa-edit"></i>Edit
    </a>
</td>
<td></td>
@else
<td>
    <a href="/task/edit/{{ $currentTask->id }}" class="btn btn-sm btn-info">
        <i class="fa fa-btn fa-edit"></i>Edit
    </a>
</td>
<td>
    <a href="/task/{{ $currentTask->id }}/subtask" class="btn btn-sm btn-primary">
        <i class="fa fa-btn fa-plus"></i>Add Sub Task
    </a>
</td>
@endif
<td>
    <form action="/task/{{ $currentTask->id }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}

        <button type="submit" class="btn btn-sm btn-danger text-center">
            <i class="fa fa-btn fa-trash"></i>
        </button>
    </form>
</td>