@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Task
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Task Form -->
                    <form action="/task" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Task Name -->
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Task</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="task-name" class="form-control" value="{{ old('task') }}">
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Task
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current Tasks -->
            @if (count($tasks) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Tasks
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                            <thead>
                                <th colspan="2">Completed?</th>
                                <th>Task</th>
                                <th colspan="3">Options</th>
                            </thead>
                            <tbody>
                                @foreach ($tasks as $task)
                                    <tr>
                                        @if (isset($edit) && isset($id) && $id == $task->id)
                                            @include('task_edit_row')
                                        @else
                                            {{-- */$currentTask=$task;/* --}}
                                            @include('task_show_row')                                           
                                        @endif
                                    </tr>
                                    @if (count($subTasks) > 0 && isset($subTasks[$task->id]))
                                        @foreach($subTasks[$task->id] as $subTask)
                                            <tr>
                                                {{-- */$currentTask=$subTask;/* --}}
                                                @if (isset($edit) && isset($child_id) && $child_id == $subTask->id)
                                                    @include('sub_task_edit_row')
                                                @else
                                                    @include('task_show_row')
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif

                                    @if(isset($taskForShowSubTaskForm) && $taskForShowSubTaskForm == $task->id)
                                        <tr>
                                            {{-- */$currentTask=$task;/* --}}
                                            @include('sub_task_edit_row')
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
